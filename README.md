# weewx-opensprinkler

The [OpenSprinkler](https://opensprinkler.com/) project relies on an
external [weather service](https://github.com/OpenSprinkler/OpenSprinkler-Weather)
to calculate a watering scale (based on temperature, humidity and previous
precipitation). 

This project aims to re-implement the OpenSprinkler weather service as a
[weeWX](http://www.weewx.com/) skin.

## Limitations

weewx-opensprinkler definitely isn't ready for serious use yet. Compare
results from this skin with what the real weather service reports, and use
it at your own risk.

* weewx-opensprinkler does not report timezone, as weeWX has no TZ support.
  It also doesn't (yet) attempt to report external IP.
* No data is read from the controller, so options for sensitivity and
  tuning the baselines are currently ignored.
* Baseline and sensitivity can be adjusted by editing parameters in
  `skin.conf`.
* Currently only manual and Zimmerman method end points are implemented.

## Installing

Installation is currently manual. Install the skin by copying the
`skins/OpenSprinkler` folder and its contents to your weeWX skins
directory (usually `/etc/weewx/skins`).

Edit `OpenSprinkler/skin.conf`. The `[Extras]` section at the top
specifies baseline parameters, and sensitivity options for the
Zimmerman method. These options match up with the options available
in the OpenSprinkler UI. Consult the
[OpenSprinkler docs](https://openthings.freshdesk.com/support/solutions/articles/5000017312-using-weather-adjustments)
for documentation on setting these.

Then add a new report to the `[StdReports]` section of the weewx
configuration. It's best to explicitly state the required units as well.
I'm using this configuration:

```
    [[OpenSprinkler]]
        skin = OpenSprinkler
        [[[Units]]]
            [[[[Groups]]]]
                group_rain = inch
                group_temperature = degree_F
```

Restart the weeWX service.

Finally, reconfigure OpenSprinkler to use the new service by browsing to
http://<opensprinkler_ip>/su and setting the "**Weather**" parameter to the
IP address or hostname of your weeWX web server.
